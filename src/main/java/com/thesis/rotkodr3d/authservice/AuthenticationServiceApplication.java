package com.thesis.rotkodr3d.authservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;


@SpringBootApplication
@EnableResourceServer
public class AuthenticationServiceApplication extends SpringBootServletInitializer {

	
	public static void main(String[] args) {
		SpringApplication.run(AuthenticationServiceApplication.class, args);
	}

	/*
	 * @RequestMapping(value="/user", produces = "application/json") public
	 * Map<String, Object> user(Authentication user) { Map<String, Object> userInfo
	 * = new HashMap<>(); userInfo.put("user", user.getPrincipal()); return
	 * userInfo; }
	 */
}

# authentication-service

This is the authentication service for the my-faculty microservice architecture.<br>
It handles the login and logout functionality. It is based on the Spring-Security-OAuth2 Framework.

## Starting the application

To start this app navigate to the project folder and execute ./mvnw spring-boot:run <br>
You can also use the app starter in your IDE, but make sure that it runs as a Spring-Boot app. <br>
<br>

The application runs under the context `auth` and uses port `8090`

## Acquiring the auth cookie
You can acquire the auth cookie from the auth service by entering the user credentials in the login form.<br>
The login form can be reached under the url localhost:8090/auth/login it's the standard login form shipped with spring-security.
<br>
The user credentials are:<br>
<br>
Username: admin<br>
Password: 123

